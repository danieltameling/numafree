# numafree

graphical free for numa nodes

## Motivation

I find the output of numactl -H hard to parse at first glance. The
information I usually want is: 1.) Is there a numa node with
significantly less total memory? This indicates usually a hardware
problem. 2.) How are large allocations distributed across the numa
nodes? 

Thus: 1.) This tool produces bars whose lengths indicate that total
memory available. (Scaled by the numa node with the most memory.) 
2.) These bars are colored according to how much memory is currently 
used on the respective numa node. For this it uses Unicode characters
U+2588 - U+258F.

## Platforms

This tool is Linux only since it relies of sysfs.

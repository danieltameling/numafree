SRC = numafree.c
OBJ = $(SRC:.c=.o)
CFLAGS = -Wall -Wmissing-prototypes -O2 -g -std=gnu18
LDFLAGS =

all: numafree

numafree: $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $(OBJ) $(LIBS)

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

sanitizer: clean
sanitizer: CFLAGS+=-fsanitize=address -fno-omit-frame-pointer -O0
sanitizer: LDFLAGS+=-fsanitize=address
sanitizer: all

clean:
	rm -f numafree $(OBJ)

.PHONY: all sanitizer clean


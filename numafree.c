/*
 *   Copyright (C) 2022 Daniel Tameling
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <inttypes.h>
#include <poll.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static const size_t max_blocks = 400;
static size_t numnodes;

typedef struct MemInfo MemInfo;
struct MemInfo {
        size_t used;
        size_t total;
};

static char *block_strings[] = {u8"\u258f", u8"\u258e", u8"\u258d", u8"\u258c", u8"\u258b", u8"\u258a", u8"\u2589"};
static char *units = "kMGTPEZY";

static void
mygetline(char **lineptr, size_t *n, FILE *stream) {
        ssize_t nread = getline(lineptr, n, stream);
        if (nread == -1 && ferror(stream))
                err(1, "getline failed");
        /* for nicer error printing */
        if (nread > 0 && (*lineptr)[nread - 1] == '\n')
                (*lineptr)[nread - 1] = '\0';
}

static size_t
mysnprintf(char *buf, size_t size, char *fmt, size_t arg) {
        int written = snprintf(buf, size, fmt, arg);
        if (written < 0)
                err(1, "snprintf failed.");
        else if (written >= size)
                errx(1, "Internal error: Tried to write %d bar_len_in_chars into a buffer of size %lu.", written, size);
        return written;
}

static size_t
strtosizet(char *str) {
        char *end;
        errno = 0;
        uintmax_t n = strtoumax(str, &end, 10);
        if (str == end)
                errno = EINVAL;
        if (n > SIZE_MAX)
                errno = ERANGE;
        return n;
}

static size_t
extract_mem_size(char *line, char *prefix, int prefixlen, char *mem_name, char *file) {
        if (strncmp(line, prefix, prefixlen) != 0)
                errx(1, "Unexpected format in file %s. Expected \"%s\" to start with \"%s\".", file, line, prefix);
        char *cur = &line[prefixlen];

        if (strncmp(cur, mem_name, strlen(mem_name)) != 0)
                errx(1, "Unexpected format in file %s. Expected \"%s\" to start with \"%s%s\".", file, line, prefix, mem_name);
        cur += strlen(mem_name);

        size_t size = strtosizet(cur);
        if (errno)
                err(1, "Failed to read the amount of memory from line \"%s\".", line);

        return size;
}

static void
pretty_print_mem_size(double mem) {
        unsigned i = 0;
        while (mem > 1000) {
                mem /= 1024.;
                i++;
        }

        if (i >= sizeof(units))
                errx(1, "Error: Value of memory is too large. Accessing the array units is out of bounds.");

        if (mem < 99.95)
                printf("%4.1f %cB", mem, units[i]);
        else
                printf("%4.0f %cB", mem, units[i]);        
}

static void
get_number_of_nodes(void) {
        char *line = NULL;
        size_t len = 0;
        ssize_t nread;
        char *path = "/sys/devices/system/node/online";

        FILE *present_file = fopen(path, "r");
        if (present_file == NULL)
                err(1, "Failed to open %s", path);

        nread = getline(&line, &len, present_file);
        /* include nread == 0 to reduce checks later on
         * nread == 0 means that nothing has been read so we can't do anything */
        if (nread <= 0) {
                if (ferror(present_file))
                        err(1, "Failed to read %s.", path);
                else
                        errx(1, "Failed to read %s.", path);
        }
        char *last_node = line + nread - 1;
        if (*last_node == '\n')
                last_node--;
        while (last_node > line && isdigit((unsigned char) last_node[-1]))
                last_node--;

        numnodes = strtosizet(last_node);
        if (errno)
                err(1, "Failed to infer the number of CPUs from \"%s\" from the file \"%s\".", line, path);
        /* the present file is zero based. */
        numnodes++;
        free(line);
        fclose(present_file);
}

int
main(int argc, char *argv[]) {
        char path[1024], prefix[1024];
        char *line = NULL;
        size_t len = 0;

        get_number_of_nodes();

        MemInfo *meminfo = calloc(numnodes, sizeof(*meminfo));

        size_t max_total = 0;
        for (size_t i = 0; i < numnodes; i++) {
                /* This makes used larger than total, which should not happen in practice */
                meminfo[i].used = (size_t) -1;

                mysnprintf(path, sizeof(path), "/sys/devices/system/node/node%lu/meminfo", i);

                FILE *meminfo_file = fopen(path, "r");
                /* assume failure means "not online" */
                if (meminfo_file == NULL)
                        continue;

                size_t prefixlen = mysnprintf(prefix, sizeof(prefix), "Node %lu Mem", i);

                mygetline(&line, &len, meminfo_file);
                size_t total = extract_mem_size(line, prefix, prefixlen, "Total:", path);
                if (total > max_total)
                        max_total = total;
             
                mygetline(&line, &len, meminfo_file);
                size_t free = extract_mem_size(line, prefix, prefixlen, "Free:", path);

                meminfo[i].total = total;
                meminfo[i].used = total - free;
        }
        free(line);

        int max_width;
        if (numnodes < 10)
                max_width = 1;
        else if (numnodes < 100)
                max_width = 2;
        else
                max_width = 3;
        
        for (size_t i = 0; i < numnodes; i++) {
                if (meminfo[i].total == 0 && meminfo[i].used == (size_t) -1)
                        continue;
                /* round the total to whole characters. At first, I
                 * drew red blocks_used with green background for the used
                 * space and then green blocks_used with no background for
                 * the free space, which allowed smooth switching from
                 * used to free space even if that occurs within a
                 * character. However, this approach had two problems:
                 * 1) If less than a complete character follows after
                 * switching to free during a character. I can't
                 * switch again. This should be rare, but I would
                 * still need a workaround. 2) Selecting makes the
                 * background color. So the red becomes green, but the
                 * green becomes black, making the bar ugly. */
                double fraction_total = (double) meminfo[i].total / (double) max_total;
                size_t bar_len_in_chars = fraction_total * (max_blocks / 8) + 0.5;
                
                double fraction_used = (double) meminfo[i].used / (double) meminfo[i].total;
                size_t blocks_used = fraction_used * (8 * bar_len_in_chars) + 0.5;
                size_t complete_chars_used = blocks_used / 8;
                size_t remaining_blocks_used = blocks_used - 8 * complete_chars_used;

                printf("Node %*lu: ", max_width, i);

                printf("\033[31;42m");
                for (size_t j = 0; j < complete_chars_used; j++)
                        printf("%s", u8"\u2588");

                if (remaining_blocks_used != 0) {
                        printf("%s", block_strings[remaining_blocks_used - 1]);
                        complete_chars_used++;
                }

                printf("\033[32;41m");
                for (size_t j = complete_chars_used; j < bar_len_in_chars; j++)
                        printf("%s", u8"\u2588");
                printf("\033[0m ");
                
                pretty_print_mem_size(meminfo[i].used);
                printf(" / ");
                pretty_print_mem_size(meminfo[i].total);
                printf("\n");
        }


        free(meminfo);
        return 0;
}
